import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PessoaComponent } from './pessoa/pessoa.component';
import { AppTopbarComponent } from './app-topbar/app-topbar.component';
import { AppMenuComponent } from './app-menu/app-menu.component';
import { CadastrarPessoaComponent } from './cadastrar-pessoa/cadastrar-pessoa.component';
import { ConsultarPessoaComponent } from './consultar-pessoa/consultar-pessoa.component';
import { PaginaInicialComponent } from './pagina-inicial/pagina-inicial.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {InputMaskModule} from 'primeng/inputmask'
import { PessoaService } from './services/pessoa.service';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import {MessageModule} from 'primeng/message';
import {MessagesModule} from 'primeng/messages';
import {TableModule} from 'primeng/table';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { EditarPessoaComponent } from './editar-pessoa/editar-pessoa.component';
import { ConfirmationService } from 'primeng/api';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { CalendarModule } from 'primeng/calendar';
import { DropdownModule } from 'primeng/dropdown';

@NgModule({
  declarations: [
    AppComponent,
    PessoaComponent,
    AppTopbarComponent,
    AppMenuComponent,
    CadastrarPessoaComponent,
    ConsultarPessoaComponent,
    PaginaInicialComponent,
    EditarPessoaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    InputMaskModule,
    HttpClientModule,
    MessagesModule,
    MessageModule,
    ToastrModule.forRoot(),
    TableModule,
    ConfirmDialogModule,
    CalendarModule,
    DropdownModule
  ],
  providers: [PessoaService, ToastrService, ConfirmationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
