import { Pessoa } from './../models/pessoa';
import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class PessoaService {

  constructor(private http: HttpClient) { }

  salvar(pessoa: Pessoa): Observable<Pessoa> {
    return this.http.post<any>('api/pessoa', pessoa);
  }

  getPessoas(): Observable<Pessoa[]> {
    return this.http.get<Pessoa[]>('api/pessoas');
  }

  getPessoa(id: number): Observable<Pessoa> {
    return this.http.get<Pessoa>('api/pessoa/' + id);
  }

  atualizar(id: number, pessoa: Pessoa): Observable<Pessoa> {
    return this.http.put<any>('api/pessoa/' + id, pessoa);
  }

  excluir(id: number) {
    return this.http.delete('api/pessoa/' + id);
  }
}
