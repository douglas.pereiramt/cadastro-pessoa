import { EditarPessoaComponent } from './editar-pessoa/editar-pessoa.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CadastrarPessoaComponent } from './cadastrar-pessoa/cadastrar-pessoa.component';
import { ConsultarPessoaComponent } from './consultar-pessoa/consultar-pessoa.component';
import { PaginaInicialComponent } from './pagina-inicial/pagina-inicial.component';

const routes: Routes = [
  {path: '', component: PaginaInicialComponent, canActivate: []},
  {path: 'cadastrar-pessoa', component: CadastrarPessoaComponent},
  {path: 'consultar-pessoas', component: ConsultarPessoaComponent},
  {path: 'editar-pessoa/:id', component: EditarPessoaComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
