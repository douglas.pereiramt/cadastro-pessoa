import { Router } from '@angular/router';
import { PessoaService } from './../services/pessoa.service';
import { Pessoa } from './../models/pessoa';
import { Component, OnInit } from '@angular/core';
import { ConfirmationService } from 'primeng/api';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-consultar-pessoa',
  templateUrl: './consultar-pessoa.component.html',
  styleUrls: ['./consultar-pessoa.component.css']
})
export class ConsultarPessoaComponent implements OnInit {

  pessoas: Pessoa[] = [];
  pessoa: Pessoa = new Pessoa();

  constructor(private pessoaService: PessoaService, private router: Router, private confirmationService: ConfirmationService, private toastrService: ToastrService) { }

  ngOnInit(): void {
    this.listar();
  }

  listar() {
    this.pessoaService.getPessoas().subscribe(res=> {
      this.pessoas = res;
    });
  }

  editarPessoa(pessoa: any) {
    this.router.navigate(['/editar-pessoa/'+ pessoa.id]);
  }

  deletarPessoa(pessoa: any) {
    this.confirmationService.confirm({
      message: "Você tem certeza que quer excluir este registro?",
      header: "Confirmação",
      icon: "pi pi-exclamation-triangle",
      accept: () => {
        this.pessoaService.excluir(pessoa.id).subscribe(res=> {
          this.pessoas = this.pessoas.filter(val=> val.id != pessoa.id);
          this.toastrService.success('Registro excluído com sucesso!');
        });
      }
    });
  }
}
