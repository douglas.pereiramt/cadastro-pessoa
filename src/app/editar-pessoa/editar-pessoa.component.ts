import { ToastrService } from 'ngx-toastr';
import { PessoaService } from './../services/pessoa.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Pessoa } from '../models/pessoa';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-editar-pessoa',
  templateUrl: './editar-pessoa.component.html',
  styleUrls: ['./editar-pessoa.component.css']
})
export class EditarPessoaComponent implements OnInit {

  id!: number;
  pessoa: Pessoa = new Pessoa();
  form!: FormGroup;
  submitedForm: boolean=false;
  
  constructor(private route: ActivatedRoute, private pessoaService: PessoaService, private formBuilder: FormBuilder, private router: Router, private toastrService: ToastrService) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    this.getPessoa();
    this.construirFormulario();
  }

  getPessoa() {
    this.pessoaService.getPessoa(this.id).subscribe(res=> {
      this.pessoa = res;
    });
  }

  isValidated(nomeCampo: string) {
    return  ((!this.form.controls[nomeCampo].valid && this.form.controls[nomeCampo].touched)
    || (!this.form.controls[nomeCampo].valid && this.submitedForm));
  }

  construirFormulario() {
    this.form = this.formBuilder.group({
      id: new FormControl(''),
      nome: new FormControl('', Validators.required),
      cpf: new FormControl('', Validators.required),
      rg: new FormControl('', Validators.required),
      dataNascimento: new FormControl('', Validators.required),
      genero: new FormControl('', Validators.required),
      estadoCivil: new FormControl('', Validators.required),
      cep: new FormControl('', Validators.required),
      uf: new FormControl('', Validators.required),
      cidade: new FormControl('', Validators.required),
      bairro: new FormControl('', Validators.required),
      endereco: new FormControl('', Validators.required),
      numero: new FormControl('', Validators.required),
      complemento: new FormControl('', Validators.required),
    });
  }

  salvar() {
    if (!this.form.valid) {
      this.submitedForm = true;
      return;
    }

    this.pessoaService.atualizar(this.id, this.pessoa).subscribe(res => {
      this.toastrService.success('Atualizado com sucesso!');
      this.router.navigate(['/consultar-pessoas']);
    })
  }

}
