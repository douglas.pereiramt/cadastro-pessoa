import { CompilePipeMetadata } from "@angular/compiler";

export class Pessoa {
    id?: number;
    nome?: string;
    cpf?: string;
    rg?: string;
    dataNascimento?: string;
    genero?: string;
    estadoCivil?: string;
    cep?: string;
    uf?: string;
    cidade?: string;
    bairro?: string;
    endereco?: string;
    numero?: number;
    complemento?: string;
}