import { utf8Encode } from '@angular/compiler/src/util';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Message, SelectItem } from 'primeng/api';
import { Pessoa } from '../models/pessoa';
import { ToastrService } from 'ngx-toastr';
import { PessoaService } from '../services/pessoa.service';

@Component({
  selector: 'app-cadastrar-pessoa',
  templateUrl: './cadastrar-pessoa.component.html',
  styleUrls: ['./cadastrar-pessoa.component.css']
})
export class CadastrarPessoaComponent implements OnInit {

  pessoa: Pessoa = new Pessoa();
  form!: FormGroup;
  submitedForm: boolean = false;
  generos: SelectItem[] = [{label: "Feminino", value: "F"}, {label: "Masculino", value: "M"}];
  estadosCivis: SelectItem[] = [{label: "Solteiro", value: "T"}, {label:"Casado", value: "C"}, {label: "Divorciado", value: "D"}, {label: "Separado", value: "P"}, {label: "Viúvo", value: "V"}];

  constructor(private formBuilder: FormBuilder, private pessoaService: PessoaService, private router: Router, private toastrService: ToastrService) {

  }

  ngOnInit(): void {
    this.construirFormulario();
  }

  construirFormulario() {
    this.form = this.formBuilder.group({
      id: new FormControl(''),
      nome: new FormControl('', Validators.required),
      cpf: new FormControl('', Validators.required),
      rg: new FormControl('', Validators.required),
      dataNascimento: new FormControl('', Validators.required),
      genero: new FormControl('', Validators.required),
      estadoCivil: new FormControl('', Validators.required),
      cep: new FormControl('', Validators.required),
      uf: new FormControl('', Validators.required),
      cidade: new FormControl('', Validators.required),
      bairro: new FormControl('', Validators.required),
      endereco: new FormControl('', Validators.required),
      numero: new FormControl('', Validators.required),
      complemento: new FormControl('', Validators.required),
    });
  }

  isValidated(nomeCampo: string) {
    return ((!this.form.controls[nomeCampo].valid && this.form.controls[nomeCampo].touched)
      || (!this.form.controls[nomeCampo].valid && this.submitedForm));
  }

  salvar() {
    if (!this.form.valid) {
      this.submitedForm = true;
      return;
    }

    this.pessoaService.salvar(this.pessoa).subscribe(res => {
      this.toastrService.success('Salvo com sucesso!');
      this.router.navigate(['/consultar-pessoas']);
    })
  }
}
